// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
	"net/http"

	"github.com/valyala/fasthttp"
)

func Middlewares[T any](mws ...func(T) T) (f T) { //middlewares
	for i := len(mws) - 1; i > -1; i-- {
		f = mws[i](f)
	}
	return
}
func main() {
	fmt.Println("Hello, 世界")

	mw1 := func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			println("before mw2")
			next(w, r) //call mw2
			println("after mw2")
		}
	}
	mw2 := func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			println("before mw3")
			next(w, r) //call mw3
			println("after mw3")
		}
	}
	mw3 := func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			println("before mw4")
			next(w, r) //call mw4
			println("after mw4")
		}
	}
	mw4 := func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			println("mw4")
		}
	}
	Middlewares(mw1, mw2, mw3, mw4)(nil, nil)
	//go http.ListenAndServe(":8080", Middlewares(mw1, mw2, mw3, mw4)) //akses: http://localhost:8080

	mw1_ := func(next fasthttp.RequestHandler) fasthttp.RequestHandler {
		return func(ctx *fasthttp.RequestCtx) {
			println("before mw2_")
			next(ctx) //call mw2_
			println("after mw2_")
		}
	}
	mw2_ := func(next fasthttp.RequestHandler) fasthttp.RequestHandler {
		return func(ctx *fasthttp.RequestCtx) {
			println("before mw3_")
			next(ctx) //call mw3_
			println("after mw3_")
		}
	}
	mw3_ := func(next fasthttp.RequestHandler) fasthttp.RequestHandler {
		return func(ctx *fasthttp.RequestCtx) {
			println("before mw4_")
			next(ctx) //call mw4_
			println("after mw4_")
		}
	}
	mw4_ := func(next fasthttp.RequestHandler) fasthttp.RequestHandler {
		return func(ctx *fasthttp.RequestCtx) {
			println("mw4_")
		}
	}
	Middlewares(mw1_, mw2_, mw3_, mw4_)(nil)
	//fasthttp.ListenAndServe(":8181", Middlewares(mw1_, mw2_, mw3_, mw4_)) //akses: http://localhost:8181
}
